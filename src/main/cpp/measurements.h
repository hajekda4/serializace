/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef MEASUREMENTS_H_744374686__H_
#define MEASUREMENTS_H_744374686__H_


#include <sstream>
#include "boost/any.hpp"
#include "avro/Specific.hh"
#include "avro/Encoder.hh"
#include "avro/Decoder.hh"

namespace eswAvro {
struct AMeasurementInfo {
    int32_t id;
    int64_t timestamp;
    std::string measurerName;
    AMeasurementInfo() :
        id(int32_t()),
        timestamp(int64_t()),
        measurerName(std::string())
        { }
};

struct ADataset {
    AMeasurementInfo info;
    std::map<std::string, std::vector<double > > records;
    ADataset() :
        info(AMeasurementInfo()),
        records(std::map<std::string, std::vector<double > >())
        { }
};

struct ADatasetArray {
    std::vector<ADataset > datasets;
    ADatasetArray() :
        datasets(std::vector<ADataset >())
        { }
};

struct AResult {
    AMeasurementInfo info;
    std::map<std::string, double > averages;
    AResult() :
        info(AMeasurementInfo()),
        averages(std::map<std::string, double >())
        { }
};

struct AResultArray {
    std::vector<AResult > results;
    AResultArray() :
        results(std::vector<AResult >())
        { }
};

struct measurements_avsc_Union__0__ {
private:
    size_t idx_;
    boost::any value_;
public:
    size_t idx() const { return idx_; }
    AMeasurementInfo get_AMeasurementInfo() const;
    void set_AMeasurementInfo(const AMeasurementInfo& v);
    ADataset get_ADataset() const;
    void set_ADataset(const ADataset& v);
    ADatasetArray get_ADatasetArray() const;
    void set_ADatasetArray(const ADatasetArray& v);
    AResult get_AResult() const;
    void set_AResult(const AResult& v);
    AResultArray get_AResultArray() const;
    void set_AResultArray(const AResultArray& v);
    measurements_avsc_Union__0__();
};

inline
AMeasurementInfo measurements_avsc_Union__0__::get_AMeasurementInfo() const {
    if (idx_ != 0) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AMeasurementInfo >(value_);
}

inline
void measurements_avsc_Union__0__::set_AMeasurementInfo(const AMeasurementInfo& v) {
    idx_ = 0;
    value_ = v;
}

inline
ADataset measurements_avsc_Union__0__::get_ADataset() const {
    if (idx_ != 1) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<ADataset >(value_);
}

inline
void measurements_avsc_Union__0__::set_ADataset(const ADataset& v) {
    idx_ = 1;
    value_ = v;
}

inline
ADatasetArray measurements_avsc_Union__0__::get_ADatasetArray() const {
    if (idx_ != 2) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<ADatasetArray >(value_);
}

inline
void measurements_avsc_Union__0__::set_ADatasetArray(const ADatasetArray& v) {
    idx_ = 2;
    value_ = v;
}

inline
AResult measurements_avsc_Union__0__::get_AResult() const {
    if (idx_ != 3) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AResult >(value_);
}

inline
void measurements_avsc_Union__0__::set_AResult(const AResult& v) {
    idx_ = 3;
    value_ = v;
}

inline
AResultArray measurements_avsc_Union__0__::get_AResultArray() const {
    if (idx_ != 4) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AResultArray >(value_);
}

inline
void measurements_avsc_Union__0__::set_AResultArray(const AResultArray& v) {
    idx_ = 4;
    value_ = v;
}

inline measurements_avsc_Union__0__::measurements_avsc_Union__0__() : idx_(0), value_(AMeasurementInfo()) { }
}
namespace avro {
template<> struct codec_traits<eswAvro::AMeasurementInfo> {
    static void encode(Encoder& e, const eswAvro::AMeasurementInfo& v) {
        avro::encode(e, v.id);
        avro::encode(e, v.timestamp);
        avro::encode(e, v.measurerName);
    }
    static void decode(Decoder& d, eswAvro::AMeasurementInfo& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.id);
                    break;
                case 1:
                    avro::decode(d, v.timestamp);
                    break;
                case 2:
                    avro::decode(d, v.measurerName);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.id);
            avro::decode(d, v.timestamp);
            avro::decode(d, v.measurerName);
        }
    }
};

template<> struct codec_traits<eswAvro::ADataset> {
    static void encode(Encoder& e, const eswAvro::ADataset& v) {
        avro::encode(e, v.info);
        avro::encode(e, v.records);
    }
    static void decode(Decoder& d, eswAvro::ADataset& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.info);
                    break;
                case 1:
                    avro::decode(d, v.records);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.info);
            avro::decode(d, v.records);
        }
    }
};

template<> struct codec_traits<eswAvro::ADatasetArray> {
    static void encode(Encoder& e, const eswAvro::ADatasetArray& v) {
        avro::encode(e, v.datasets);
    }
    static void decode(Decoder& d, eswAvro::ADatasetArray& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.datasets);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.datasets);
        }
    }
};

template<> struct codec_traits<eswAvro::AResult> {
    static void encode(Encoder& e, const eswAvro::AResult& v) {
        avro::encode(e, v.info);
        avro::encode(e, v.averages);
    }
    static void decode(Decoder& d, eswAvro::AResult& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.info);
                    break;
                case 1:
                    avro::decode(d, v.averages);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.info);
            avro::decode(d, v.averages);
        }
    }
};

template<> struct codec_traits<eswAvro::AResultArray> {
    static void encode(Encoder& e, const eswAvro::AResultArray& v) {
        avro::encode(e, v.results);
    }
    static void decode(Decoder& d, eswAvro::AResultArray& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.results);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.results);
        }
    }
};

template<> struct codec_traits<eswAvro::measurements_avsc_Union__0__> {
    static void encode(Encoder& e, eswAvro::measurements_avsc_Union__0__ v) {
        e.encodeUnionIndex(v.idx());
        switch (v.idx()) {
        case 0:
            avro::encode(e, v.get_AMeasurementInfo());
            break;
        case 1:
            avro::encode(e, v.get_ADataset());
            break;
        case 2:
            avro::encode(e, v.get_ADatasetArray());
            break;
        case 3:
            avro::encode(e, v.get_AResult());
            break;
        case 4:
            avro::encode(e, v.get_AResultArray());
            break;
        }
    }
    static void decode(Decoder& d, eswAvro::measurements_avsc_Union__0__& v) {
        size_t n = d.decodeUnionIndex();
        if (n >= 5) { throw avro::Exception("Union index too big"); }
        switch (n) {
        case 0:
            {
                eswAvro::AMeasurementInfo vv;
                avro::decode(d, vv);
                v.set_AMeasurementInfo(vv);
            }
            break;
        case 1:
            {
                eswAvro::ADataset vv;
                avro::decode(d, vv);
                v.set_ADataset(vv);
            }
            break;
        case 2:
            {
                eswAvro::ADatasetArray vv;
                avro::decode(d, vv);
                v.set_ADatasetArray(vv);
            }
            break;
        case 3:
            {
                eswAvro::AResult vv;
                avro::decode(d, vv);
                v.set_AResult(vv);
            }
            break;
        case 4:
            {
                eswAvro::AResultArray vv;
                avro::decode(d, vv);
                v.set_AResultArray(vv);
            }
            break;
        }
    }
};

}
#endif
