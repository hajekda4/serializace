#include <iostream>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <cstring>
#include <unistd.h>


#include <jsoncpp/json/json.h>


#include "measurementinfo.h"
#include "dataset.h"
#include "result.h"

//#include <boost/array.hpp>
#include <boost/asio.hpp>

#include "measurements.pb.h"

#include "measurements.h"
#include "avro/Encoder.hh"
#include "avro/Decoder.hh"

using namespace std;
using boost::asio::ip::tcp;

void processJSON(tcp::iostream& stream){
    Json::Value val;
    Json::Reader reader;

    std::vector<Dataset> datasets;
    std::vector<Result> results;

    /* Read json string from the stream */
    string s;
    getline(stream, s, '\0');

    /* Parse string */
    reader.parse(s, val);

    datasets.clear();
    results.clear();
    for (int i = 0; i < val.size(); i++) {
        datasets.emplace_back();
        datasets[i].Deserialize(val[i]);
        /* Calculate averages */
        results.emplace_back(datasets[i].getInfo(), datasets[i].getRecords());
    }

    /* Create output JSON structure */
    Json::Value out;
//    Json::FastWriter writer;
    Json::StyledWriter writer;
    for (int i = 0; i < results.size(); i++) {
        Json::Value result;
        results[i].Serialize(result);
        out[i] = result;
    }

    /* Send the result back */
    std::string output = writer.write(out);
    stream << output;
    cout << output;
}

int readAndDecodeMessageSize(tcp::iostream& stream) {
    char buffer[4];
    stream.read(buffer, 4);
    return int((unsigned char)(buffer[0]) << 24 |
               (unsigned char)(buffer[1]) << 16 |
               (unsigned char)(buffer[2]) << 8 |
               (unsigned char)(buffer[3]));
}

void processAvro(tcp::iostream& stream){

    int messageSize = readAndDecodeMessageSize(stream);
    char *buffer = new char[messageSize];
    stream.read(buffer, messageSize);

    unique_ptr<avro::InputStream> in = avro::memoryInputStream((const uint8_t*) buffer, messageSize);
    avro::DecoderPtr d = avro::binaryDecoder();
    d->init(*in);

    eswAvro::ADatasetArray aDatasetArray;
    avro::decode(*d, aDatasetArray);

    eswAvro::AResultArray aResultArray;

    for(auto & dataset : aDatasetArray.datasets) {
        eswAvro::AResult result;
        result.info = dataset.info;
        const auto averages = &result.averages;
        for (auto & record : dataset.records) {
            double count = 0;

            for (auto & measure : record.second)
                count += measure;

            (*averages)[record.first] = count/record.second.size();
        }

        aResultArray.results.push_back(result);
    }

    unique_ptr<avro::OutputStream> out = avro::memoryOutputStream();
    avro::EncoderPtr e = avro::binaryEncoder();
    e->init(*out);

    avro::encode(*e, aResultArray);

    auto snapshot = avro::snapshot(*out);
    stream.write(reinterpret_cast<const char*>(snapshot->data()), snapshot->size());

    delete[] buffer;
}

void processProtobuf(tcp::iostream& stream){

    int messageSize = readAndDecodeMessageSize(stream);
    char *buffer = new char[messageSize];
    stream.read(buffer, messageSize);

    esw::PDatasetArray pDatasetArray;
    pDatasetArray.ParseFromArray(buffer, messageSize);

    esw::PResultArray pResultArray;

    for (int i=0; i < pDatasetArray.datasets_size(); i++) {
        const esw::PDatasetArray_PDataset& dataset = pDatasetArray.datasets(i);

        esw::PResultArray_PResult* result = pResultArray.add_results();
        esw::PResultArray_PResult_PMeasurementInfo* result_measurement = result->mutable_info();
        result_measurement->set_id(dataset.info().id());
        result_measurement->set_timestamp(dataset.info().timestamp());
        result_measurement->set_measurername(dataset.info().measurername());

        auto averages = result->mutable_averages();
        auto records = dataset.records();
        for (auto & record : records){
            double count = 0;
            esw::PDatasetArray_PDataset_PListOfDoubles& listOfDoubles = record.second;
            double numOfDoubles = listOfDoubles.double__size();
            for(int j=0; j < numOfDoubles; ++j){
                count += listOfDoubles.double_(j);
            }

            (*averages)[record.first] = count/numOfDoubles;
        }
    }

    int writeSize = pResultArray.ByteSizeLong();
    char *writeBuffer = new char[writeSize];
    pResultArray.SerializeToArray(writeBuffer, writeSize);
    stream.write(writeBuffer, writeSize);

    google::protobuf::ShutdownProtobufLibrary();
    delete[] buffer;
}

int main(int argc, char *argv[]) {

    if (argc != 3) {
        cout << "Error: two arguments required - ./server  <port> <protocol>" << endl;
        return 1;
    }



    // unsigned short int port = 12345;
    unsigned short int port = atoi(argv[1]);

    // std::string protocol = "json";
    std::string protocol(argv[2]);
    try {
        boost::asio::io_service io_service;

        tcp::endpoint endpoint(tcp::v4(), port);
        tcp::acceptor acceptor(io_service, endpoint);

        while (true) {
            tcp::iostream stream;
            boost::system::error_code ec;
            acceptor.accept(*stream.rdbuf(), ec);

            if(protocol == "json"){
                processJSON(stream);
            }else if(protocol == "avro"){
                processAvro(stream);
            }else if(protocol == "proto"){
                processProtobuf(stream);
            }else{
                throw std::logic_error("Protocol not yet implemented");
            }

        }

    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
