package cz.esw.serialization.handler;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.json.*;
import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.avro.*;
import cz.esw.serialization.json.DataType;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * @author Marek Cuchý (CVUT)
 */
public class AvroDataHandler implements DataHandler {

	private static final ObjectMapper MAPPER;

	static{
		//prevent socket from closing on write and read JSON
		JsonFactory factory = new JsonFactory();
		factory.disable(JsonGenerator.Feature.AUTO_CLOSE_TARGET);
		factory.disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);
		MAPPER = new ObjectMapper(factory);
	}

	private final InputStream is;
	private final OutputStream os;

	protected Map<Integer, ADataset> datasets;

	public AvroDataHandler(InputStream is, OutputStream os) {
		this.is = is;
		this.os = os;
		datasets = new HashMap<Integer, ADataset>();
	}

	@Override
	public void initialize() {

	}

	@Override
	public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
		ADataset dataset = new ADataset();
		dataset.setInfo(new AMeasurementInfo(datasetId, timestamp, measurerName));
		//dataset.setRecords(new EnumMap<Enum, List<Double>>());
		dataset.setRecords(new TreeMap<CharSequence, List<Double>>());
		this.datasets.put(datasetId, dataset);
	}

	@Override
	public void handleValue(int datasetId, DataType type, double value) {
		ADataset dataset = datasets.get(datasetId);
		if (dataset == null) {
			throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
		}
		dataset.getRecords().computeIfAbsent(type.toString(), t -> new ArrayList<>()).add(value);
	}

	@Override
	public void getResults(ResultConsumer consumer)  throws IOException, IllegalArgumentException {
		//convert datasets to JSON and write them to the output stream
		MAPPER.writeValue(os, datasets.values().toArray());
		os.write(0); //write end

		//parse JSON results from the input stream
		AResult[] results = MAPPER.readValue(is, AResult[].class);
		//AResult[] results = new AResult[0];

		for (AResult result : results) {
			AMeasurementInfo info = result.getInfo();
			consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName().toString());

			for (Map.Entry<CharSequence, Double> entry : result.getAverages().entrySet()) {
				DataType dt = DataType.valueOf(entry.getKey().toString());
				double v = entry.getValue();
				consumer.acceptResult(dt,v);
			}
		}
	}
}
